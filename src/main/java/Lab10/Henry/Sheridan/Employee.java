/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Lab10.Henry.Sheridan;
import java.util.Date;

/**
 *
 * @author Henry Echefu
 */
public class Employee {
    private String employeeId;
    private String name;
    private Date dateOfJoining;
    private double salary;

  
    
    
    public Employee(){}
    
    public Employee(String employeeId, String name, Date dateOfJoining, double salary){
          this.employeeId =  employeeId;
          this.name = name;
          this.dateOfJoining = dateOfJoining;
          this.salary = salary;
    }

    
    
    public String getEmployeeId(){
        return this.employeeId;
    }
    
    public String getEmployeeName(){
        return this.name;
    }
     public Date getEmployeeDateOfJoining(){
        return this.dateOfJoining;
     }
    
      public double getEmployeeSalary() {
        return salary;
    }
    
    
    public boolean isPromotionDueThisYear(){
        return false;
    }
}


